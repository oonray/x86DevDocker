FROM ubuntu:latest
RUN apt-get update
RUN apt-get install nasm -y 
RUN apt-get install gcc g++ -y
RUN apt-get install -y python python3 python3-pip python-pip
RUN pip3 install numpy pandas sklearn tensorflow scapy flask flask-sqlalchemy
RUN apt-get install -y gdb radare2
RUN touch /etc/sysctl.d/01-disable-aslr.conf
RUN echo "kernel.randomize_va_space = 0" > /etc/sysctl.d/01-disable-aslr.conf
CMD /usr/bin/bash